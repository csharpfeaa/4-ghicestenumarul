﻿using System;

namespace GhicesteNumarul
{
    class Program
    {
        /*
         * Ghiciti numarul random ales de calculator
         * 
         * Cerinte
         * ---
         * Numarul aleator sa fie de la 0 la 100
         * Cereti un numar de la ultilizator pana cand ghiceste numarul ales
         * 
         * Pentru fiecare incercare afisati cat de aproape este:
         * <=3 - foarte fierbinte
         * <= 5 - fierbinte
         * <= 10 - cald
         * <= 20 - caldut
         * <= 50 - rece
         * foarte rece
         * 
         * 2. Afisati numarul de incercari 
         */
        static void Main(string[] args)
        {
            // generam un numar aleator mai mic ca 100
            Random rand = new Random();
            int numarAles = rand.Next(100);

            int numarUtilizator;
            bool ghicit = false;
            int incercari = 0;

            while (ghicit == false)
            {
                // cerem un numar de la utilizator
                Console.Write("Ghiciti numarul: ");

                while (int.TryParse(Console.ReadLine(), out numarUtilizator) == false || numarUtilizator > 100)
                {
                    Console.WriteLine("Numarul introdus nu este valid.");
                    Console.Write("Intorduceti un nou numar: ");
                }

                // incrementam numarul de incercari
                incercari++;


                // verificam daca a ghicit numarul
                if (numarUtilizator == numarAles)
                {
                    Console.WriteLine("Felicitari, ati ghicit numarul din " + incercari + " incercari!");
                    ghicit = true;
                }
                else
                {
                    // afisam cat de aproape este
                    int diferentaAbs = Math.Abs(numarAles - numarUtilizator);

                    if (diferentaAbs <= 3)
                    {
                        Console.WriteLine("Foarte fierbinte");
                    }
                    else if (diferentaAbs <= 5)
                    {
                        Console.WriteLine("Fierbinte");
                    }
                    else if (diferentaAbs <= 10)
                    {

                        Console.WriteLine("Cald");
                    }
                    else if (diferentaAbs <= 20)
                    {
                        Console.WriteLine("Caldut");
                    }
                    else if (diferentaAbs <= 50)
                    {
                        Console.WriteLine("Rece");
                    }
                    else
                    {
                        Console.WriteLine("Foarte rece");
                    }
                }

                Console.WriteLine();
            }


            Console.ReadKey();
        }
    }
}